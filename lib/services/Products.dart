import 'dart:convert';

import 'package:flutter_app/Model/User.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/Model/categories.dart';

class ProductService {
  Future<List<Products>> searchProduct() async {
    String url = "http://192.168.0.107:49154/api/Product/Search";

    final response = await http.get(url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        //"Authorization": tokenID
      },);
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      var data = response.body;
      return productsFromJson(data);
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<List<Products>> searchProductLikes() async {
    String url = "http://192.168.0.107:49154/api/Product/SearchProductLikes";

    final response = await http.get(url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": AuthData.token
      },);
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      var data = response.body;
      return productsFromJson(data);
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<bool> likeProduct(tokenID, Likes product) async {
    String url = "http://192.168.0.107:49154/api/Product/LikeProduct";
    final response = await http.post(url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": tokenID
      },
      body: likesToJson(product)
    );
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return true;
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<String> checkLiked(tokenID, productID) async {
     String url = "http://192.168.0.107:49154/api/Product/CheckLiked";
    final response = await http.get(
        Uri.parse(url + '/' + productID),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": tokenID
        },
    );
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return response.body;
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<bool> deleteLiked(tokenID, productID) async {
    String url = "http://192.168.0.107:49154/api/Product/DeleteLike";
    final response = await http.delete(
      Uri.parse(url + '/' + productID),
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": tokenID
      },
    );
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return true;
    } else {
      throw Exception('Failed to load data!');
    }
  }
}