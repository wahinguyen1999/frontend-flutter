import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_app/Model/categories.dart';

class CategoryService {
  Future<List<Categories>> searchCategory() async {
    String url = "http://192.168.0.107:49154/api/Category/Search";

    final response = await http.get(url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        //"Authorization": tokenID
      },);
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      var data = response.body;
      return categoriesFromJson(data);
    } else {
      throw Exception('Failed to load data!');
    }
  }
}