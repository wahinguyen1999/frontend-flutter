import 'package:flutter_app/Model/orders.dart';
import 'package:http/http.dart' as http;

class OrderService {
  Future<bool> Order(tokenID, Orders order) async {
    String url = "http://192.168.0.107:49154/api/Product/Order";
    final response = await http.post(url,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": tokenID
        },
        body: ordersToJson(order)
    );
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return true;
    } else {
      throw Exception('Failed to load data!');
    }
  }
}