import 'package:http/http.dart' as http;
import 'dart:convert';
import '../Model/User.dart';

class AuthService {
  Future<AuthDataModel> login(LoginRequestModel requestModel) async {
    String url = "http://192.168.0.107:49153/api/Authentication/Login";

    final response = await http.post(url,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json"
        },
        body: loginRequestModelToJson(requestModel));
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return authDataModelFromJson(response.body);
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<bool> register(RegisterModel requestModel) async {
    String url = "http://192.168.0.107:49153/api/Authentication/Register";

    final response = await http.post(url,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json"
        },
        body: registerModelToJson(requestModel));
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return true;
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<AuthData> getDataUser(tokenID) async {
    String url = "http://192.168.0.107:49153/api/Authentication/AuthData";

    final response = await http.get(url,
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          "Authorization": tokenID
        },);
    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      var data = response.body;
      return AuthData.fromJson(json.decode(data));
    } else {
      throw Exception('Failed to load data!');
    }
  }

  Future<bool> updateDataUser(tokenID, UpdateAuthDataModel dataUpdate) async {
    String url = "http://192.168.0.107:49153/api/Authentication/UpdateAuthData";

    final response = await http.put(url,
      headers: {
        "Accept": "application/json",
        "content-type": "application/json",
        "Authorization": tokenID
      },
      body: updateAuthDataModelToJson(dataUpdate)
    );

    int error = response.statusCode;
    print("Status: $error");
    if (response.statusCode == 200 || response.statusCode == 400) {
      return true;
    } else {
      throw Exception('Failed to load data!');
    }
  }
}
