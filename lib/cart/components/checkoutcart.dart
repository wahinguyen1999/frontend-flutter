import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/carts.dart';
import 'package:flutter_app/Model/orders.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/homepage/homepage.dart';
import 'package:flutter_app/services/Orders.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckOutCart extends StatelessWidget {
  double sum;
  var token;
  CheckOutCart({this.sum, this.token});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[

        Expanded(
          child: FlatButton(
            height: 50,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                side: BorderSide(color: Colors.green)),
            color: Colors.white,
            textColor: Colors.green,
            onPressed: () {},
            child: Text(
              "Tổng: ${sum}",
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
          ),
        ),

        Expanded(child: FlatButton(
          height: 50,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            OrderService orderService = new OrderService();
            Orders order = new Orders();
            order.priceTotal = sum;
            print(token);
            orderService.Order(token, order).then((value){
              print(value);
            });
            Cart.cart = [];
            Navigator.pushNamed(context, HomePage.routeName);
            Fluttertoast.showToast(
                msg: "Đặt hàng thành công",
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 20.0
            );
            //Navigator.pop(context);
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text("Thanh toán".toUpperCase(),
              style: TextStyle(fontSize: 14)),
        ),)
      ],
    );
  }
}
