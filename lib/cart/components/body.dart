import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/User.dart';
import 'package:flutter_app/cart/components/checkoutcart.dart';
import 'package:flutter_app/Model/carts.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Products> cartdetails = Cart().getCart();
  double sum =0.0;
  String token = AuthData.token;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cartdetails.forEach((product) { sum = sum + product.price; });
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: cartdetails.length,
                itemBuilder: (context, index)  {
                  return Column(
                    children: [
                      GestureDetector(
                        child: CartItem(product: cartdetails[index],),
                        onTap: (){
                          setState(() {
                            cartdetails.removeAt(index);
                            sum = 0.0;
                            cartdetails.forEach((product) { sum = sum + product.price; });
                          });
                        },
                      ),
                      Divider()
                    ],
                  ) ;
                }),
          ),
          CheckOutCart(sum: sum,token: token)
        ],
      ),
    );
  }
}

class CartItem extends StatelessWidget {
  Products product;

  CartItem({this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF5F5F5),
      padding: EdgeInsets.all(16),
      child: Row(
          children: [
            SizedBox(
                width: 100,
                height: 90,
                child: Image.network(product.image)),
            Expanded(child:
            Text('${product.productName}',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0,
                  letterSpacing: 1,
                )),),
            Expanded(child: Text(product.price.toString())) ,
            Icon(Icons.delete_outlined)
          ]

      ),
    );
  }
}
