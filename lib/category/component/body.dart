import 'package:flutter/material.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/homepage/components/homeheader.dart';
import 'package:flutter_app/homepage/components/menuheader.dart';

import 'categoryform.dart';

class Body extends StatelessWidget {
  int categoryID;
  List<Products> products;
  Body({this.categoryID, this.products});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: HomeHeader(),
        title: MenuHeader(),
      ),
      body: CategoryForm(id : this.categoryID, products: products),
    );
  }
}
