import 'package:flutter/material.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/detail/productpage.dart';

class CategoryForm extends StatelessWidget {
  List<Products> products;
  int id;
  CategoryForm({this.id, this.products});

  List<Products> getPfromCate(int id){
    List<Products> tmp = [];
    for(int i=0; i < products.length; i++){
      if(products[i].categoryId == id){
        tmp.add(products[i]);
      }
    }
    return tmp;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ListView.builder(
          padding: const EdgeInsets.all(10),
          itemCount: getPfromCate(id).length,
          itemBuilder: (context, index) {
            return ListTile(
            onTap: () {
              Navigator.pushNamed(context, ProductPage.routeName,
                  arguments: ProductDetailsArguments(product: getPfromCate(id)[index]));
            },
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10),),
              leading: Image.network( getPfromCate(id)[index].image,
                height: 150,
                width: 80,
              ),
              //trailing: Icon(Icons.api_outlined),
              focusColor: Colors.black,
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(getPfromCate(id)[index].productName,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        letterSpacing: 1,
                        backgroundColor: Colors.lightBlueAccent
                      )),
                  Text("${getPfromCate(id)[index].price.toString()} VND",
                        style: TextStyle(
                          height: 2
                        )),
                  //Text(getPfromCate(id)[index].description, overflow: TextOverflow.ellipsis),
                ],
              ),
            );
          }),
    );
  }
}

