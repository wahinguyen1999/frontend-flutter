import 'package:flutter/material.dart';
import 'package:flutter_app/homepage/components/fragment/home_fragment_categories..dart';

import 'component/body.dart';



class CategoryPage extends StatelessWidget {
  static String routeName = "/category_screen";
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as ScreenArguments;
    return Body(categoryID: args.categoryID, products: args.products);
  }
}
