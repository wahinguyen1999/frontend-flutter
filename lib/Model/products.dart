import 'package:flutter/material.dart';
import 'package:flutter_app/Model/User.dart';
import 'dart:convert';

import 'package:flutter_app/services/Products.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<Products> productsFromJson(String str) => List<Products>.from(json.decode(str).map((x) => Products.fromJson(x)));

String productsToJson(List<Products> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Products {
  Products({
    this.productId,
    this.categoryId,
    this.productName,
    this.price,
    this.image,
    this.description,
    this.featured,
  });

  int productId;
  int categoryId;
  String productName;
  double price;
  String image;
  String description;
  int featured;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
    productId: json["productID"],
    categoryId: json["categoryID"],
    productName: json["productName"],
    price: json["price"],
    image: json["image"],
    description: json["description"],
    featured: json["featured"],
  );

  Map<String, dynamic> toJson() => {
    "productID": productId,
    "categoryID": categoryId,
    "productName": productName,
    "price": price,
    "image": image,
    "description": description,
    "featured": featured,
  };

  static List<Products> init() {
    ProductService productService = new ProductService();
    List<Products> data = [];
    productService.searchProduct().then((value) {
      // print(value);
      data.addAll(value);
    });
    return data;
  }

  static List<Products> listLiked() {
    ProductService productService = new ProductService();
    List<Products> data = [];
    productService.searchProductLikes().then((value) {
      print(value);
      data.addAll(value);
    });
    return data;
  }
}

Likes likesFromJson(String str) => Likes.fromJson(json.decode(str));

String likesToJson(Likes data) => json.encode(data.toJson());

class Likes {
  Likes({
    this.productId,
  });

  int productId;

  factory Likes.fromJson(Map<String, dynamic> json) => Likes(
    productId: json["productID"],
  );

  Map<String, dynamic> toJson() => {
    "productID": productId,
  };
}
