import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:flutter_app/services/Categories.dart';

// class Categories {
//   int id;
//   String title;
//   String image;
//
//   Categories({this.id, this.title, this.image});
//
//   static List<Categories> init() {
//     List<Categories> data = [
//       Categories(
//           id: 1, title: "HighLand", image: "assets/images/ic_highland.jpeg"),
//       Categories(
//           id: 2, title: "HighLand", image: "assets/images/ic_circlek.png"),
//       Categories(
//           id: 3, title: "HighLand", image: "assets/images/ic_ministop.png"),
//       Categories(
//           id: 4, title: "HighLand", image: "assets/images/ic_seveneleven.png"),
//       Categories(
//           id: 5, title: "HighLand", image: "assets/images/ic_vinmart.jpg"),
//     ];
//     return data;
//   }
// }



List<Categories> categoriesFromJson(String str) => List<Categories>.from(json.decode(str).map((x) => Categories.fromJson(x)));

String categoriesToJson(List<Categories> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Categories {

  Categories({
    this.categoryId,
    this.categoryName,
    this.imageUrl,
  });

  int categoryId;
  String categoryName;
  String imageUrl;

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
    categoryId: json["categoryID"],
    categoryName: json["categoryName"],
    imageUrl: json["imageURL"],
  );

  Map<String, dynamic> toJson() => {
    "categoryID": categoryId,
    "categoryName": categoryName,
    "imageURL": imageUrl,
  };

  static List<Categories> init() {
    CategoryService categoryService = new CategoryService();
    List<Categories> data = [];
    categoryService.searchCategory().then((value) {
      data.addAll(value);
    });
    // data = [
    //   Categories(
    //       categoryId: 1, categoryName: "HighLand", imageUrl: "assets/images/ic_highland.jpeg"),
    //   Categories(
    //     categoryId: 2, categoryName: "HighLand", imageUrl: "assets/images/ic_highland.jpeg"),
    // ];
    return data;
  }
}
