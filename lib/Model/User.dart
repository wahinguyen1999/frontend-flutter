import 'dart:convert';

// class User{
//   String username;
//   String password;
//
//   User({this.username, this.password});
// }

LoginRequestModel loginRequestModelFromJson(String str) => LoginRequestModel.fromJson(json.decode(str));

String loginRequestModelToJson(LoginRequestModel data) => json.encode(data.toJson());

class LoginRequestModel {
  LoginRequestModel({
    this.userName,
    this.password,
  });

  String userName;
  String password;

  factory LoginRequestModel.fromJson(Map<String, dynamic> json) => LoginRequestModel(
    userName: json["userName"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName,
    "password": password,
  };
}




AuthDataModel authDataModelFromJson(String str) => AuthDataModel.fromJson(json.decode(str));

String authDataModelToJson(AuthDataModel data) => json.encode(data.toJson());

class AuthDataModel {
  AuthDataModel({
    this.tokenId,
    this.authData,
  });

  String tokenId;
  AuthData authData;

  factory AuthDataModel.fromJson(Map<String, dynamic> json) => AuthDataModel(
    tokenId: json["tokenID"],
    authData: AuthData.fromJson(json["authData"]),
  );

  Map<String, dynamic> toJson() => {
    "tokenID": tokenId,
    "authData": authData.toJson(),
  };
}

class AuthData {
  static var token = "";

  AuthData({
    this.userId,
    this.fullName,
    this.userName,
    this.password,
  });

  int userId;
  String fullName;
  String userName;
  String password;

  factory AuthData.fromJson(Map<String, dynamic> json) => AuthData(
    userId: json["userID"],
    fullName: json["fullName"],
    userName: json["userName"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "userID": userId,
    "fullName": fullName,
    "userName": userName,
    "password": password,
  };
}

RegisterModel registerModelFromJson(String str) => RegisterModel.fromJson(json.decode(str));

String registerModelToJson(RegisterModel data) => json.encode(data.toJson());

class RegisterModel {
  RegisterModel({
    this.fullName,
    this.userName,
    this.password,
  });

  String fullName;
  String userName;
  String password;

  factory RegisterModel.fromJson(Map<String, dynamic> json) => RegisterModel(
    fullName: json["fullName"],
    userName: json["userName"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "fullName": fullName,
    "userName": userName,
    "password": password,
  };
}

UpdateAuthDataModel updateAuthDataModelFromJson(String str) => UpdateAuthDataModel.fromJson(json.decode(str));

String updateAuthDataModelToJson(UpdateAuthDataModel data) => json.encode(data.toJson());

class UpdateAuthDataModel {
  UpdateAuthDataModel({
    this.fullName,
    this.userName,
    this.password,
  });

  String fullName;
  String userName;
  String password;

  factory UpdateAuthDataModel.fromJson(Map<String, dynamic> json) => UpdateAuthDataModel(
    fullName: json["fullName"],
    userName: json["userName"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "fullName": fullName,
    "userName": userName,
    "password": password,
  };
}


