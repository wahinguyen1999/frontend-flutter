import 'dart:convert';

Orders ordersFromJson(String str) => Orders.fromJson(json.decode(str));

String ordersToJson(Orders data) => json.encode(data.toJson());

class Orders {
  Orders({
    this.priceTotal,
  });

  double priceTotal;

  factory Orders.fromJson(Map<String, dynamic> json) => Orders(
    priceTotal: json["priceTotal"],
  );

  Map<String, dynamic> toJson() => {
    "priceTotal": priceTotal,
  };
}