import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/services/Auth.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_app/SignUp/SignUpPage.dart';
import 'package:flutter_app/homepage/homepage.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/Routes/routes.dart';
import 'package:flutter_app/Model/User.dart';

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {

  final Shader linearGradient = LinearGradient(
    colors: <Color>[Colors.blue[200], Colors.purple],
  ).createShader(Rect.fromLTWH(100.0, 100.0, 250.0, 70.0));

  final Shader linearGradient1 = LinearGradient(
    colors: <Color>[Colors.blue[800], Colors.purple],
  ).createShader(Rect.fromLTWH(50.0, 150.0, 100.0, 0.0));


  final _formKey = GlobalKey<FormState>();
  bool _value = false;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  bool isApiCallProcess = false;
  LoginRequestModel requestModel;
  //final username = TextEditingController();
  //final password = TextEditingController();
  FToast fToast;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestModel = new LoginRequestModel();
    _getData();
  }

  _getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //prefs.setString('token', "");
    if(prefs.getString('token') == ""){
      print("No token");
    }
    else{
      print(prefs.getString('token'));
      AuthData.token = prefs.getString('token');
      Navigator.pushNamed(context, HomePage.routeName);
    }
  }


  @override
  Widget build(BuildContext context) {

    return Form(
      key: globalFormKey,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Smile Shop", style: TextStyle(fontSize: 32,
                        foreground: Paint()..shader=linearGradient,
                                                        fontWeight: FontWeight.bold),)
                  ],
                )),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextFormField(
                     validator: (value){
                       return Utilities.validateEmail(value);
                     },
                      onSaved: (input) => requestModel.userName = input,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: "Username",
                          prefixIcon: Icon(Icons.mail_outline)
                      ),),
                    SizedBox(height: 15,),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      validator: (value){
                        return Utilities.validatePassword(value);
                      },
                      onSaved: (input) => requestModel.password = input,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: "Password",
                          prefixIcon: Icon(Icons.lock_outline_rounded)
                      ),),
                    SizedBox(height: 15,),
                    Row(
                      children: [
                        Checkbox(value: _value?? true, onChanged: (value)  {
                          setState((){
                            _value = value;
                          });
                        }),
                        Text("Remember me", style: TextStyle(fontSize: 16, foreground: Paint()..shader=linearGradient,),)
                      ],
                    ),
                    SizedBox(height: 5,),
                    SizedBox(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        onPressed: () async {
                          AuthService apiService = new AuthService();
                          if(validateAndSave()){
                            print(requestModel.toJson());
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            apiService.login(requestModel).then((value) {
                              if(value.tokenId.isNotEmpty) {
                                if(_value == true) {
                                  prefs.setString('username', requestModel.userName);
                                  prefs.setString('password', requestModel.password);
                                  prefs.setBool('check', _value);
                                }
                                else {
                                  prefs.remove('check');
                                }
                                prefs.setString('token', value.tokenId);
                                Navigator.pushNamed(context, HomePage.routeName);
                              }
                              else {
                                Fluttertoast.showToast(
                                    msg: "Username or Password wrong",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.TOP,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                            });
                          }
                        },
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40),),
                        color: Color.fromRGBO(164, 218, 251, 1),
                        child: Text("Sign In", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),),
                    ),
                    SizedBox(height: 5,),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      // child: Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: [
                      //     Container(
                      //       height: 40,
                      //       width: 40,
                      //       padding: EdgeInsets.all(10),
                      //       decoration: BoxDecoration(
                      //           color: Color(0xFFF5F6F9),
                      //           shape: BoxShape.circle
                      //       ),
                      //       child: SvgPicture.asset("assets/icons/facebook-2.svg"),
                      //     ),
                      //     Container(
                      //       height: 40,
                      //       width: 40,
                      //       margin: EdgeInsets.only(left: 10, right: 10),
                      //       padding: EdgeInsets.all(10),
                      //       decoration: BoxDecoration(
                      //           color: Color(0xFFF5F6F9),
                      //           shape: BoxShape.circle
                      //       ),
                      //       child: SvgPicture.asset("assets/icons/google-icon.svg"),
                      //     ),
                      //     Container(
                      //       height: 40,
                      //       width: 40,
                      //       padding: EdgeInsets.all(10),
                      //       decoration: BoxDecoration(
                      //           color: Color(0xFFF5F6F9),
                      //           shape: BoxShape.circle
                      //       ),
                      //       child: SvgPicture.asset("assets/icons/twitter.svg"),
                      //     )
                      //   ],
                      // ),
                    ),
                    SizedBox(height: 5,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Don't have an account ? ", style: TextStyle(foreground: Paint()..shader=linearGradient, fontSize: 14),),
                        GestureDetector(
                            onTap: () async {
                              await Navigator.pushNamed(context, SignUpPage.routeName);
                              //User user = result;
                              //username.text = user.username ;
                            },
                            child: Text(" Sign Up", style: TextStyle(decoration:TextDecoration.underline,color: Colors.red, fontSize: 14), ))

                      ],
                    )

                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
