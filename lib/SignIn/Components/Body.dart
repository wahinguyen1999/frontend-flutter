import 'package:flutter/material.dart';
import 'package:flutter_app/SignIn/Components/SignIn_Form.dart';
import 'package:flutter/cupertino.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            headerScreen(context),
            SignInForm(),
          ],
        ),
      ),
    ));
  }

  Widget headerScreen(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 80.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height*0.2 ,
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Image.asset("discord.png"),
    );
  }
}
