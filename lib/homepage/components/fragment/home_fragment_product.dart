import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/detail/productpage.dart';

class ProductPopular extends StatelessWidget {
  List<Products> products;
  ProductPopular({this.products});
  //List<Products> products = Products.init();
  @override
  Widget build(BuildContext context) {
    List<Products> popularProducts = products.where((element) => element.featured == 1).toList();
    //var productsAPI = Utilities().getProducts();
    print(products);

    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                'Sản phẩm nổi bật',
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.black45),
              )),
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: popularProducts.length,
                  itemBuilder: (context, index) {
                    return ProductItem(
                      product: popularProducts[index],
                    );
                  })),
        ],),
      ),
    );
  }
}

// class ProductItem extends StatelessWidget {
//   Products product;
//
//   ProductItem({this.product});
//
//   @override
//   Widget build(BuildContext context) {
//     if (product.image != null) {}
//     return GestureDetector(
//       onTap: () {
//         Utilities.data.add(product);
//         Navigator.pushNamed(context, ProductPage.routeName,
//           arguments: ProductDetailsArguments(product: product));
//       },
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Image.network(
//             product.image,
//             fit: BoxFit.fill,
//           ),
//           Row(
//             children: [
//               Expanded(child: Text(product.productName)),
//               Container(
//                   padding: EdgeInsets.all(2),
//                   decoration: BoxDecoration(
//                       border: Border.all(color: Colors.white),
//                       borderRadius: BorderRadius.circular(2),
//                       color: Colors.green),
//                   child: Text(
//                     product.price.toString(),
//                     style: TextStyle(
//                         color: Colors.white, fontWeight: FontWeight.bold),
//                   )),
//             ],
//           )
//         ],
//       ),
//     );
//   }

  class ProductItem extends StatelessWidget {
    Products product;
    ProductItem({this.product});

    @override
    Widget build(BuildContext context) {
      return GestureDetector(
        onTap: (){
          Navigator.pushNamed(context, ProductPage.routeName, arguments: ProductDetailsArguments(product: product));
        },
        child: Container(
          margin: const EdgeInsets.only(right: 20.0),
          decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          border: Border.all(color: Color.fromRGBO(164, 218, 251, 1), width: 3.0),
        ),
        width: 190,
        height: 200,
        padding: EdgeInsets.all(20), //Image.network(product.image),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 150 ,
              child: Image.network(product.image)
          ),
          SizedBox(height: 10,),
          Expanded(child:
            Text('${product.productName}',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  letterSpacing: 1,
                )),),
          Expanded(child:
              Text('${product.price} VND',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 15.0,
                  letterSpacing: 0,
              )),
          ),
          ])
    ),
  );
  }
}
