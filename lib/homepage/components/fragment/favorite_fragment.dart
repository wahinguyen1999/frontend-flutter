import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/detail/productpage.dart';

class FavoriteDetail extends StatelessWidget {
  List<Products> products;
  FavoriteDetail({this.products});

  @override
  Widget build(BuildContext context) {
    print(products.length.toString());
    return Container(
      child: Expanded(
        child: ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              return ProductItemList(
                product: products[index],
              );
            }),
      ),
    );
  }
}

class ProductItemList extends StatelessWidget {
  Products product;

  ProductItemList({this.product});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GestureDetector(
        onTap: (){
          Navigator.pushNamed(context, ProductPage.routeName, arguments: ProductDetailsArguments(product: product));
        },
        child: Container(
            margin: const EdgeInsets.only(right: 250.0, left: 0.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Color.fromRGBO(164, 218, 251, 1), width: 5.0),
            ),
            height: 190,
            padding: EdgeInsets.all(10), //Image.network(product.image),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                      height: 100 ,
                      child: Image.network(product.image)
                  ),
                  SizedBox(height: 10,),
                  Expanded(child:
                  Text('${product.productName}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        letterSpacing: 1,
                      )),),
                  Expanded(child:
                  Text('${product.price} VND',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15.0,
                        letterSpacing: 0,
                      )),
                  ),
                ])
        ),
      ),
    );
  }
}
