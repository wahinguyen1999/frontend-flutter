import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/categories.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/category/categorypage.dart';
import 'package:flutter_app/services/Categories.dart';

class ScreenArguments {
  int categoryID;
  List<Products> products;

  ScreenArguments(this.categoryID, this.products);
}

class CategoriesStore extends StatelessWidget {
  List<Categories> categories;
  List<Products> products;
  CategoriesStore({this.categories, this.products});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Text('Danh mục',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.black45),
                )),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 150,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: categories.length,
                  itemBuilder: (context, index) {
                    return CategoriesItem(category: categories[index], products: products,);
                  }),
            )
          ],
        ),
      ),
    );
  }
}

class CategoriesItem extends StatelessWidget {
  Categories category;
  List<Products> products;

  CategoriesItem({this.category, this.products});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, CategoryPage.routeName, arguments: ScreenArguments(category.categoryId, products));
      },
      child: Container(
        margin: const EdgeInsets.only(right: 20.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            border: Border.all(color: Color.fromRGBO(164, 218, 251, 1), width: 3.0),
        ),
        width: 200,
        height: 150,
        padding: EdgeInsets.all(25),
        child: Image.network(category.imageUrl),
      ),
    );
  }
}
