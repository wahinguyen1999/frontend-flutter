import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/User.dart';
import 'package:flutter_app/SignIn/SignInPage.dart';
import 'package:flutter_app/homepage/homepage.dart';
import 'package:flutter_app/services/Auth.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AccountDetail extends StatefulWidget {
  @override
  _AccountDetailState createState() => _AccountDetailState();
}
var data;

class _AccountDetailState extends State<AccountDetail> {
  var fullNameController = new TextEditingController();
  var userNameController = new TextEditingController();
  var passwordController = new TextEditingController();
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  UpdateAuthDataModel requestModel;
  @override
  void initState() {
    // TODO: implement initState
    requestModel = new UpdateAuthDataModel();
    super.initState();
    _getDataUser();
  }

  _getDataUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    AuthService authService = new AuthService();
    authService.getDataUser(prefs.getString('token')).then((value){
      data = value;
      fullNameController.text = data.fullName;
      userNameController.text = data.userName;
      passwordController.text = data.password;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: globalFormKey,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(2),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Xin chào", style: TextStyle(fontSize: 30,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold),)
                  ],
            )),
            SizedBox(height: 20,),
            fullNameTextFormField(),
            SizedBox(height: 30,),
            emailTextFormField(),
            SizedBox(height: 30,),
            passwordTextFormField(),
            SizedBox(height: 30,),
            SizedBox(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  AuthService apiService = new AuthService();
                  if(validateAndSave()){
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    requestModel.fullName = fullNameController.text;
                    requestModel.userName = userNameController.text;
                    requestModel.password = passwordController.text;

                    apiService.updateDataUser(prefs.getString('token'), requestModel).then((value) {
                      if(value == true) {
                        print("Update Success");
                      }
                      else{
                        print("Update Fail");
                      }
                    });
                  }
                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40),),
                color: Color.fromRGBO(164, 218, 251, 1),
                child: Text("Lưu", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),),),
            ),
            SizedBox(height: 20,),
            SizedBox(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  print(prefs.getString('token'));
                  prefs.setString('token', "");
                  Navigator.pushNamed(context, SignInPage.routeName);
                  //Navigator.pop(context);
                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40),),
                color: Color.fromRGBO(164, 218, 251, 1),
                child: Text("Đăng xuất", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),),),
            ),
            SizedBox(height: 30,),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       Container(
            //         height: 40,
            //         width: 40,
            //         padding: EdgeInsets.all(10),
            //         decoration: BoxDecoration(
            //             color: Color(0xFFF5F6F9),
            //             shape: BoxShape.circle
            //         ),
            //         child: SvgPicture.asset("assets/icons/facebook-2.svg"),
            //       ),
            //       Container(
            //         height: 40,
            //         width: 40,
            //         margin: EdgeInsets.only(left: 10, right: 10),
            //         padding: EdgeInsets.all(10),
            //         decoration: BoxDecoration(
            //             color: Color(0xFFF5F6F9),
            //             shape: BoxShape.circle
            //         ),
            //         child: SvgPicture.asset("assets/icons/google-icon.svg"),
            //       ),
            //       Container(
            //         height: 40,
            //         width: 40,
            //         padding: EdgeInsets.all(10),
            //         decoration: BoxDecoration(
            //             color: Color(0xFFF5F6F9),
            //             shape: BoxShape.circle
            //         ),
            //         child: SvgPicture.asset("assets/icons/twitter.svg"),
            //       )
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
  TextFormField fullNameTextFormField() {
    return TextFormField(
      controller: fullNameController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Enter your full name ",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.drive_file_rename_outline)
      ),
      //validator: Utilities.validateEmail,
      // onSaved:(value){
      //   setState(() {
      //     fullName.text = value;
      //   });
      // },
    );
  }

  TextFormField emailTextFormField() {
    return TextFormField(
      controller: userNameController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Enter your email ",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.email_outlined)
      ),
    );
  }

  TextFormField passwordTextFormField() {
    return TextFormField(
      controller: passwordController,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Enter your password",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.lock_outline)
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
