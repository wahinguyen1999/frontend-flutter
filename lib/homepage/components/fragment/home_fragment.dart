import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/categories.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/homepage/components/fragment/home_fragment_categories..dart';
import 'package:flutter_app/services/Categories.dart';

// import 'home_fragment_categories.dart';
import 'home_fragment_product.dart';

class HomeDetail extends StatelessWidget {
  List<Categories> categories;
  List<Products> products;
  HomeDetail({this.categories, this.products});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: [
          CategoriesStore(categories: categories, products: this.products),
          ProductPopular(products: products,)
        ],
      ),
    );
  }
}
