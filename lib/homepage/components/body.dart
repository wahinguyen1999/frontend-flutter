import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/User.dart';
import 'package:flutter_app/Model/carts.dart';
import 'package:flutter_app/Model/categories.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/cart/cartpage.dart';
import 'package:flutter_app/detail/productpage.dart';

import 'package:flutter_app/homepage/components/fragment/account_detail.dart';
import 'package:flutter_app/homepage/components/fragment/favorite_fragment.dart';
import 'package:flutter_app/homepage/components/fragment/home_fragment.dart';

import 'package:flutter_app/homepage/components/homeheader.dart';
import 'package:flutter_app/homepage/components/fragment/notification_fragment.dart';
import 'package:flutter_app/homepage/homepage.dart';
import 'package:flutter_app/services/Categories.dart';
import 'package:flutter_app/services/Products.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'menuheader.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}
List<Categories> categories = Categories.init();
List<Products> products = Products.init();

class _BodyState extends State<Body> {
  var selectIndex = 0;
  var flag = true;
  List<Products> productsLike = Products.listLiked();
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   ProductService productService = new ProductService();
  //   productService.searchProductLikes().then((value) {
  //     productsLike.addAll(value);
  //   });
  // }
  int total;

  @override
  Widget build(BuildContext context) {
    List<Widget> screen = [
      HomeDetail(categories: categories, products: products),
      FavoriteDetail(products: productsLike),
      AccountDetail(),
    ];
    total = Cart.cart.length;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        //title: flag ? HomeHeader() : MenuHeader(),
        title: Container(
          margin: const EdgeInsets.only(top: 5.0),
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset("discord.png"),
        ),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {
            showSearch(context: context, delegate: DataSearch());
          }),
          Container(
            height: 40,
            width: 40,
            child: Badge(
                position: BadgePosition.topEnd(top: 0, end: 0),
                badgeContent:  Text(
                    total.toString(),style: TextStyle(color: Colors.white, fontSize: 10)),
                child:
                IconButton(icon: Icon(Icons.shopping_cart),onPressed: (){
                  final result = Navigator.pushNamed(context, CartPage.routeName);
                  result.then((value){
                    setState(() {
                      total = Cart.cart.length;
                    });
                  },);
                })
            ),
          ),
        ],
      ),
      //drawer: Drawer(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectIndex,
        onTap: (index) {
          setState(() {
            selectIndex = index;
            if (selectIndex != 2) {
              flag = true;
            } else {
              flag = false;
            }
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Trang chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Yêu thích',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Tài khoản',
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            screen[selectIndex]
          ],
        ),
      ),
    );
  }
}

class DataSearch extends SearchDelegate<String>{

  final searchProducts = products;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(icon: Icon(Icons.clear), onPressed: () {
      query = "";
    })];
    // TODO: implement buildActions
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(icon: AnimatedIcon(
      icon: AnimatedIcons.menu_arrow,
      progress: transitionAnimation,),
      onPressed: (){
        close(context, null);
      });
    // TODO: implement buildLeading
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
        ? []
        : searchProducts.where((p) => p.productName.startsWith(query)).toList();

    return ListView.builder(
    padding: const EdgeInsets.all(10),
    itemBuilder: (context, index) => ListTile(
      onTap: () {
        Navigator.pushNamed(context, ProductPage.routeName,
            arguments: ProductDetailsArguments(product: suggestionList[index]));
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10),),
      trailing: Icon(Icons.wb_incandescent_outlined),
      leading: Image.network( suggestionList[index].image, height: 200, width: 70,),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(text: TextSpan(
            text: suggestionList[index].productName.substring(0, query.length),
            style: TextStyle(color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              ),
            children: [
              TextSpan(
                text: suggestionList[index].productName.substring(query.length),
                style: TextStyle(color: Colors.grey),),
            ]
          ),),
          Text("${suggestionList[index].price.toString()} VND",
            style: TextStyle(
            height: 2
          )),
        ],
      ),
    ),
      itemCount: suggestionList.length,
    );
    // TODO: implement buildSuggestions
  }

}
