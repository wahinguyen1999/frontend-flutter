import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MenuHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 90,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            //color: Colors.green
        ),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(6.0),
                      alignment: Alignment.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 5.0),
                            width: 35,
                            height: 35,
                            alignment: Alignment.center,
                            child: Image.asset("discord.png"),
                          ),
                          Text("Smile Shop", style: TextStyle(
                              fontSize: 15,
                              color: Color.fromRGBO(164, 218, 251, 1),
                              fontWeight: FontWeight.bold),),
                        ],
                  )),
              ]
              // children: [
              //   Expanded(child: Text("Account info", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),)),
              //   GestureDetector(
              //     onTap: (){
              //     },
              //     child: Text("Save", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),),
              //   )
              // ],
    )));
  }
}
