import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/carts.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/detail/productpage.dart';
import 'package:flutter_app/services/Products.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddProductToCart extends StatefulWidget {
  Products product;
  AddProductToCart({this.product});
  @override
  _AddProductToCartState createState() => _AddProductToCartState();
}

class _AddProductToCartState extends State<AddProductToCart> {
  Likes likes;
  bool _isVisible;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  _getLike() async {
    likes.productId = widget.product.productId;
    String query = '${widget.product.productId}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    ProductService productService = new ProductService();
    productService.checkLiked(prefs.getString('token'), query).then((value){
      print(value);
      if(value == 'true') {
        _isVisible = true;
      }
      if(value == 'false') {
        _isVisible = false;
      }
      setState(() {
        _isVisible = _isVisible;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    likes = new Likes();
    _getLike();
  }


  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 200,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            if (_isVisible == true) RaisedButton(
              onPressed: () async {
                String query = '${widget.product.productId}';
                SharedPreferences prefs = await SharedPreferences.getInstance();
                ProductService productService = new ProductService();
                productService.deleteLiked(prefs.getString('token'), query).then((value) {
                  if(value == true) {
                    Fluttertoast.showToast(
                        msg: "Đã bỏ thích",
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 20.0
                    );
                    setState(() {
                      _isVisible = false;
                    });
                  }
                });
              },
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              color: Colors.red,
              child: Text("Bỏ yêu thích", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),),
            if (_isVisible == false) RaisedButton(
              onPressed: () async {
                likes.productId = widget.product.productId;
                SharedPreferences prefs = await SharedPreferences.getInstance();
                ProductService productService = new ProductService();
                productService.likeProduct(prefs.getString('token'), likes).then((value) {
                  if(value == true) {
                    Fluttertoast.showToast(
                        msg: "Đã thích",
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 20.0
                    );
                    setState(() {
                      _isVisible = true;
                    });
                  }
                });
              },
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              color: Colors.red,
              child: Text("Yêu thích", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),),
            RaisedButton(
              onPressed: () {
                Cart cart = Cart();
                cart.addProductToCart(widget.product);
                print(widget.product.productId);
                Fluttertoast.showToast(
                    msg: "Đã thêm vào giỏ",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 20.0
                );
              },
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              color: Colors.green,
              child: Text("Thêm vào giỏ hàng", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),),),
          ],
        ),
    );
  }
}
