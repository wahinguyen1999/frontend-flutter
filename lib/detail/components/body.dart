import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/detail/components/addtocart.dart';
import 'package:flutter_app/Model/products.dart';


class Body extends StatelessWidget {
  Products product;
  Body({this.product});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 250 ,
              child: Image.network(product.image)
          ),
          SizedBox(height: 40,),
          RichText(text: TextSpan(
              text: ("Tên sản phẩm:").substring(0, 13),
              style: TextStyle(color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
              children: [
                TextSpan(
                  text: " ${product.productName}",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,),)
              ]
          ),),
          SizedBox(height: 10.0,),
          RichText(text: TextSpan(
              text: ("Giá tiền:").substring(0, 9),
              style: TextStyle(color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
              children: [
                TextSpan(
                  text: " ${product.price} VNĐ",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal,),)
              ]
          ),),
          SizedBox(height: 10.0,),
          RichText(text: TextSpan(
              text: ("Mô tả:").substring(0, 6),
              style: TextStyle(color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
              children: [
                TextSpan(
                  text: " ${product.description}",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, height: 1.4),)
              ]
          ),),
          SizedBox(height: 20.0,),
          AddProductToCart(product: product)
        ],
      ),
    );
  }
}
