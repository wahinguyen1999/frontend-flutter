import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/SignIn/SignInPage.dart';

class SplashPage extends StatelessWidget {
  static String routeName = "/splash";


  @override
  Widget build(BuildContext context) {
    new Future.delayed(new Duration(seconds: 4), () {
      Navigator.pushNamedAndRemoveUntil(context, SignInPage.routeName, (Route<dynamic> route) => false);
    });
    final Shader linearGradient = LinearGradient(
      colors: <Color>[Colors.blue[200], Colors.purple],
    ).createShader(Rect.fromLTWH(100.0, 0.0, 250.0, 70.0));

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: Color.fromRGBO(164, 218, 251, 1)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 150,
                height: 150,
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue[200]),
                      // backgroundColor: Colors.purple[200],
                      strokeWidth: 15,
                    ),
                    Center(child:

                    Text('Loading...', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,foreground: Paint()..shader=linearGradient),)
                    ),
                  ],
                ),
              ),

            ],
          ) ,),
      ),
    );
  }
}
