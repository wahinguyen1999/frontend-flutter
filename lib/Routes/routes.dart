import 'package:flutter/widgets.dart';
import 'package:flutter_app/SignUp/SignUpPage.dart';
import 'package:flutter_app/homepage/homepage.dart';
import 'package:flutter_app/SignIn/SignInPage.dart';
import 'package:flutter_app/Routes/SplashPage.dart';
import 'package:flutter_app/cart/cartpage.dart';
import 'package:flutter_app/detail/productpage.dart';
import 'package:flutter_app/category/categorypage.dart';



final Map<String, WidgetBuilder> routes = {
  SplashPage.routeName : (context) => SplashPage(),
  SignInPage.routeName : (context) => SignInPage(),
  SignUpPage.routeName : (context) => SignUpPage(),
  HomePage.routeName : (context) => HomePage(),
  ProductPage.routeName : (context) => ProductPage(),
  CartPage.routeName: (context) => CartPage(),
  CategoryPage.routeName: (context) => CategoryPage()
};
