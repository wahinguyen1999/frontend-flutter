import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/SignUp/Components/SignUp_Form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            headerScreen(context),
            Text("Smile Shop", style: TextStyle(fontSize: 32,
                color: Color.fromRGBO(164, 218, 251, 1),
                fontWeight: FontWeight.bold),),
            SignUpForm()
          ],
        ),),
    ));
  }
  Widget headerScreen(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height*0.2 ,
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Image.asset("discord.png"),
    );
  }
}
