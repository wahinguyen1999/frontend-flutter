import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/Model/User.dart';
import 'package:flutter_app/Model/utilities.dart';
import 'package:flutter_app/services/Auth.dart';
import 'package:flutter_svg/svg.dart';


class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  // var email = TextEditingController();
  // var fullName = TextEditingController();
  // final password = TextEditingController();
  final conform = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  var _passKey = GlobalKey<FormFieldState>();

  RegisterModel registerModel = new RegisterModel();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(height: 20,),
            fullNameTextFormField(),
            SizedBox(height: 20,),
            emailTextFormField(),
            SizedBox(height: 20,),
            passwordTextFormField(),
            SizedBox(height: 20,),
            conformTextFormField(),
            SizedBox(height: 30,),
            SizedBox(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  print(registerModel.toJson());
                  if(validateAndSave())
                  {
                    AuthService authService = new AuthService();
                    authService.register(registerModel).then((value){
                      if(value == true) {
                        Navigator.pop(context, LoginRequestModel(userName: registerModel.userName, password: registerModel.password));
                      }
                    });
                  }
                  // if(_formKey.currentState.validate()){
                  //   Navigator.pop(context, User(username: email.text, password: conform.text));
                  // }
                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40),),
                color: Color.fromRGBO(164, 218, 251, 1),
                child: Text("Sign Up", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),),
            ),
            SizedBox(height: 30,),
            Container(
              width: MediaQuery.of(context).size.width,
              // child: Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Container(
              //       height: 40,
              //       width: 40,
              //       padding: EdgeInsets.all(10),
              //       decoration: BoxDecoration(
              //           color: Color(0xFFF5F6F9),
              //           shape: BoxShape.circle
              //       ),
              //       child: SvgPicture.asset("assets/icons/facebook-2.svg"),
              //     ),
              //     Container(
              //       height: 40,
              //       width: 40,
              //       margin: EdgeInsets.only(left: 10, right: 10),
              //       padding: EdgeInsets.all(10),
              //       decoration: BoxDecoration(
              //           color: Color(0xFFF5F6F9),
              //           shape: BoxShape.circle
              //       ),
              //       child: SvgPicture.asset("assets/icons/google-icon.svg"),
              //     ),
              //     Container(
              //       height: 40,
              //       width: 40,
              //       padding: EdgeInsets.all(10),
              //       decoration: BoxDecoration(
              //           color: Color(0xFFF5F6F9),
              //           shape: BoxShape.circle
              //       ),
              //       child: SvgPicture.asset("assets/icons/twitter.svg"),
              //     )
              //   ],
              // ),
            ),
          ],
        ),
      ),
    );
  }

  TextFormField fullNameTextFormField() {
    return TextFormField(
      //controller: fullName,
      onSaved: (input) => registerModel.fullName = input,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Enter your full name ",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.drive_file_rename_outline)
      ),
      //validator: Utilities.validateEmail,
      // onSaved:(value){
      //   setState(() {
      //     fullName.text = value;
      //   });
      // },
    );
  }

  TextFormField emailTextFormField() {
    return TextFormField(
      //controller: email,
      onSaved: (input) => registerModel.userName = input,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Enter your email ",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.email_outlined)
      ),
      validator: Utilities.validateEmail,
      // onSaved:(value){
      //   setState(() {
      //     email.text = value;
      //   });
      // },
    );
  }



  TextFormField passwordTextFormField() {
    return TextFormField(
        key: _passKey,
        onSaved: (input) => registerModel.password = input,
        //controller: password,
        obscureText: true,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "Enter your password",
            // If  you are using latest version of flutter then lable text and hint text shown like this
            // if you r using flutter less then 1.20.* then maybe this is not working properly
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons.lock_outline)
        ),
        validator: (passwordKey){
          return Utilities.validatePassword(passwordKey);
        }
    );
  }

  TextFormField conformTextFormField() {
    return TextFormField(
      controller: conform,
      obscureText: true,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Re-enter your password",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.lock_outline)
      ),
      validator:  (conformPassword) {
        var pass = _passKey.currentState.value;
        return Utilities.conformPassword(conformPassword, pass);
      },
      onSaved: (value){
        setState(() {
          conform.text = value;
        });

      },

    );
  }
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
